<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Ordem extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'ordens';

    protected $guarded = [];

    public function cliente() {
        return $this->belongsTo(Cliente::class);
    }
    // Retorna o create_at em um formato mais legível. O método anterior criava conflitos já que retornava um novo objeto Carbon com formato diferente que conflitava com o banco de dados
    public function criadoEm(){
        return $this->created_at->format('j F, Y');
    }
}   
