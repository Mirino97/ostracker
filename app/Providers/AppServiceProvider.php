<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\GoogleAgendaServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GoogleAgendaHelper::class, function ($app) {
            return new GoogleAgendaServiceProvider();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
