<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use App\Models\Ordem;
use Google_Client;
use DateTime;
use DateInterval;

class GoogleController extends Controller
{
    public function redirectToAuthUrl() {
        $client = new Google_Client();
        $client->setAuthConfig(resource_path('client_secret.json'));
        $client->setRedirectUri('http://127.0.0.1:8000/authenticate');
        $client->addScope('https://www.googleapis.com/auth/calendar.events');
        $client->setAccessType('offline');
        $auth_url = $client->createAuthUrl();
        
        return redirect($auth_url);
    }

    public function authenticateGoogleInstance() {
        $client = new Google_Client();
        $client->setAuthConfig(resource_path('client_secret.json'));
        $client->setRedirectUri('http://127.0.0.1:8000/authenticate');
        $client->addScope('https://www.googleapis.com/auth/calendar.events');
        // Use fetchAccessTokenWithAuthCode instead of just autenthicate() so you can force the API to respond you with actual error messages you case you fudge something up.
        $access_token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $currentTime = new DateTime('NOW');
        $expiresInSeconds = $access_token['expires_in'];
        $expiryDate = $currentTime->add(new DateInterval('PT'.$expiresInSeconds.'S'));
        $access_token['expiryDate'] = $expiryDate->format('Y-m-d H:i:s');

        Storage::put('public/access_token.json', json_encode($access_token));

        return redirect()->route('home');
    }

    public function createCalendarEvent(Request $request) {
        try {
            $novaOrdem = Ordem::where('id', '=', $request->novaOrdem)->first();
            $client = new Google_Client();
            $client->setApplicationName('OSTracker');
            $client->setScopes('https://www.googleapis.com/auth/calendar');
            $client->setAuthConfig(resource_path('client_secret.json'));
            $client->setAccessType('offline');
            $client->setApprovalPrompt("consent");

            // Token de acesso retirado de um documento local. Provavelmente não é muito seguro, procurar solução melhor para quando ir live.
            $retrieved = json_decode(file_get_contents(storage_path('app/public/access_token.json')));

            $now = new DateTime('NOW');
            $expiry = new DateTime($retrieved->expiryDate);

            if ($now > $expiry) {
                $request = Http::post('https://oauth2.googleapis.com/token', [
                    'client_id' => env('CLIENT_ID'),
                    'client_secret' => env('CLIENT_SECRET'),
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $retrieved->refresh_token
                ]);

                if ($request->successful()) {
                    $expiryDate = $now->add(new DateInterval('PT'.$request['expires_in'].'S'));
                    $retrieved->expiryDate = $expiryDate->format('Y-m-d H:i:s');
                    $retrieved->access_token = $request['access_token'];

                    Storage::put('public/access_token.json', json_encode($retrieved));
                } else {
                    return response()->json('Houve um problema com a re-autenticação do token de acesso do Google.');
                }
            }

            $access_token = $retrieved->access_token;
            $client->setAccessToken($access_token);

            $service = new \Google_Service_Calendar($client);
            
            // O CÓDIGO ABAIXO PREPARA O HORÁRIO DE INÍCIO E FIM DO EVENTO A SER CRIADO. É NECESSÁRIO DAR ESSE CONTROLE AO USUÁRIO AO INVÉS DE DEIXAR HARDCODED.
            $pattern = "/(\d{2})\/(\d{2})\/(\d{4})/";
            $str = '$3-$2-$1T09:00:00-10:00';
            $startDateTime = preg_replace($pattern, '$3-$2-$1T09:00:00-10:00', $novaOrdem->data_atendimento);
            $endDateTime = preg_replace($pattern, '$3-$2-$1T10:00:00-11:00', $novaOrdem->data_atendimento);

            $event = new \Google_Service_Calendar_Event(array(
                'summary' => $novaOrdem->nome_cliente,
                'location' => $novaOrdem->endereco,
                'description' => $novaOrdem->descricao,
                // START E END SÃO NECESSÁRIOS
                'start' => array(
                    'dateTime' => $startDateTime,
                    'timeZone' => 'America/Los_Angeles',
                ),
                'end' => array(
                    'dateTime' => $endDateTime,
                    'timeZone' => 'America/Los_Angeles',
                    ),
                'reminders' => array(
                    'useDefault' => FALSE,
                    'overrides' => array(
                    array('method' => 'popup', 'minutes' => 10),
                    ),
                ),
            ));
            
            $calendarId = 'primary';
            $event = $service->events->insert($calendarId, $event);
            $eventCreated = true;
            $message = ['class' => 'success', 'text' => 'OS criada e Evento adicionado com sucesso!'];
            return response()->json(['novaOrdem' => $novaOrdem, 'message' => $message]);
        } catch (\Throwable $th) {
            return response()->json("Oops, something went wrong!".$th);
        }
        
    }
}
