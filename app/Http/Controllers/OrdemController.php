<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Ordem;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;

class OrdemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ordens.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ordens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $decoded = json_decode($request->getContent(), true);
            $validator = Validator::make($decoded, [
                'nome_cliente' => 'required',
                'servico' => 'nullable',
                'data_atendimento' => 'nullable',
                'valor' => 'nullable',
                'estado' => 'required',
                'descricao' => 'nullable',
                'observacao' => 'nullable',
                'endereco' => 'nullable',
            ]);

            if ($validator->fails()) {
                $message = ['class' => 'error', 'text' => 'O validator recusou os parâmetros informados'];
                return response()->json($message, 406);
            }

            $novaOrdem = Ordem::create($decoded);
            $eventCreated = null;

            $cliente = Cliente::where('nome', $decoded['nome_cliente'])->first();
            $cliente->ordens()->save($novaOrdem);

            $novaOrdem['isNew'] = true;

            if ($novaOrdem->estado == "Agendado") {
                return redirect()->route('createCalendarEvent', compact('novaOrdem'));
                
            } else {
                $message = ['class' => 'success', 'text' => 'OS criada com sucesso!'];
                return response()->json(['novaOrdem' => $novaOrdem, 'message' => $message]);
            }

            /* if ($eventCreated = true) {
                $message = ['class' => 'success', 'text' => 'OS criada e Evento adicionado com sucesso!'];
            } else {
                $message = ['class' => 'success', 'text' => 'OS criada com sucesso!'];
            } */
        } catch (\Throwable $th) {
            $message = ['class' => 'error', 'text' => 'Algo deu errado: '.$th];
            return response()->json($message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $ordens = Ordem::orderBy('created_at', 'desc')->get();
        return response()->json($ordens);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ordem $ordem)
    {
        return view('ordens.update', compact('ordem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $decoded = json_decode($request->getContent(), true);
            $ordem = Ordem::findOrFail($decoded['id']);

            $validator = Validator::make($decoded, [
                'nome_cliente' => 'required',
                'servico' => 'nullable',
                'data_atendimento' => 'nullable',
                'valor' => 'nullable',
                'estado' => 'required',
                'descricao' => 'nullable',
                'observacao' => 'nullable',
                'endereco' => 'nullable',
            ]);

            if ($validator->fails()) {
                $message = ['class' => 'error', 'text' => 'O validator não aceitou os parâmetros informados. Por favor, tente novamente!'];
                return response()->json($message, 406);
            };
            
            $ordem->update($decoded);

            $message = ['class' => 'success', 'text' => 'OS atualizada com sucesso!'];
            return response()->json(['ordem' => $ordem, 'message' => $message]);
        } catch (\Throwable $th) {
            Session::flash('error', 'Oops, algo deu errado! Envie essa mensagem para seu administrador de sistema: '.$th);
            return response()->json($th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $decoded = json_decode($request->getContent(), true);
            $ordemDeletada = Ordem::destroy($decoded);
            $message = ['class' => 'success', 'text' => 'Ordem deletada com sucesso!'];
            return response()->json(['message' => $message , 'clienteDeletado' => $decoded]);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage());
        }
        
    }
}
