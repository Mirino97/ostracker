<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ordem;
use App\Models\Cliente;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        $this->middleware('auth');
    } */

    public function show()
    {
        $ordens = Ordem::get();
        $ordensPagas = Ordem::where('estado', '=','Pago')->get();
        $ordensAbertas = Ordem::where('estado', '=', 'Aberto')->get();
        $ordensAgendadas = Ordem::where('estado', '=', 'Agendado')->get();
        $clientes = Cliente::get()->count();

        return response()->json(['ordens' => $ordens, 'ordensAbertas' => $ordensAbertas, 'ordensPagas' => $ordensPagas, 'ordensAgendadas' => $ordensAgendadas, 'clientes' => $clientes]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
