<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
class ClienteController extends Controller
{

    public function search(Request $request) {
        $pesquisa = json_decode($request->getContent(), true);
        $clientes = Cliente::select('nome', 'endereco')->where('nome', 'LIKE', '%'.$pesquisa.'%')->get();
        return response()->json($clientes);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $clientes = Cliente::with('ordens')->get();
        return response()->json($clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Cliente::create($request->validate([
                'nome' => 'required',
                'cpf' => 'nullable',
                'endereco' => 'nullable',
                'email' => 'nullable',
                'telefone' => 'nullable'
            ]));
            Session::flash('success', 'Cliente cadastrado com sucesso!');
        } catch (\Throwable $th) {
            Session::flash('error', 'Ops, algo deu errado! Envie essa mensagem para seu administrador de sistema: '.$th->message);
        }

        return redirect()->route('cliente.index');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        return view('clientes.update', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $decoded = json_decode($request->getContent(), true);
            $cliente = Cliente::findOrFail($decoded['id']);
            $validator = Validator::make($decoded, [
                'nome' => 'required',
                'cpf' => 'nullable',
                'endereco' => 'nullable',
                'email' => 'nullable',
                'telefone' => 'nullable'
            ]);
            
            if ($validator->fails()) {
                $message = ['class' => 'error', 'text' => 'O validator recusou os parâmetros informados'];
                return response()->json($message, 406);
            };

            $cliente->update($decoded);
            $message = ['class' => 'success', 'text' => 'Cliente atualizado com sucesso!'];
            return response()->json(['cliente' => $cliente, 'message' => $message]);

        } catch (\Throwable $th) {
            $message = ['class' => 'error', 'text' => 'Opa! Algo deu errado. Por favor, tente novamente mais tarde. Erro do código: '.$th];
            return response()->json($message, 500);
        }

        return redirect()->route('cliente.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $clienteId = json_decode($request->getContent(), true);
            Cliente::destroy($clienteId);

            $message = ['class' => 'success', 'text' => 'Cliente deletado com sucesso!'];
            return response()->json(['message' => $message, 'clienteDeletado' => $clienteId]);
        } catch (\Throwable $th) {
            $message = ['class' => 'error', 'text' => 'Opa, algo deu errado: '.$th];
            return response()->json($message);
        }
    }
}
