<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/financeiro', App\Http\Controllers\FinanceiroController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/googleAuth', [App\Http\Controllers\GoogleController::class, 'redirectToAuthUrl']);
Route::get('/authenticate', [App\Http\Controllers\GoogleController::class, 'authenticateGoogleInstance']);
Route::get('/createCalendarEvent', [App\Http\Controllers\GoogleController::class, 'createCalendarEvent'])->name('createCalendarEvent');
