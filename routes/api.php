<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Rotas de ordens
Route::get('/ordens', [App\Http\Controllers\OrdemController::class, 'show']);
Route::post('/ordem/create', [App\Http\Controllers\OrdemController::class, 'store']);
Route::post('/ordem/update', [App\Http\Controllers\OrdemController::class, 'update']);
Route::delete('/ordem/destroy', [App\Http\Controllers\OrdemController::class, 'destroy']);

// Rotas de clientes
Route::get('/clientes', [App\Http\Controllers\ClienteController::class, 'index']);
Route::post('/cliente/search', [App\Http\Controllers\ClienteController::class, 'search']);
Route::post('/cliente/update', [App\Http\Controllers\ClienteController::class, 'update']);
Route::delete('/cliente/destroy', [App\Http\Controllers\ClienteController::class, 'destroy']);

// Rotas Google
/* Route::get('/googleAuth', [App\Http\Controllers\GoogleController::class, 'redirectToAuthUrl']);
Route::get('/authenticate', [App\Http\Controllers\GoogleController::class, 'authenticateGoogleInstance']); */


// Rotas home
Route::get('/home', [App\Http\Controllers\HomeController::class, 'show']);
