require('./bootstrap');

import router from './router/index.js'
import flashMessage from './components/FlashMessage'
import modal from './components/Modal'

window.Event = new Vue();

let vue = new Vue({
    router,
    el: '#app',
    components: {flashMessage, modal}
});
