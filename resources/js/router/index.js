// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.
import listaOrdem from '../components/ordens/ListaOrdem'
import listaClientes from '../components/clientes/ListaClientes'
import homeView from '../components/HomeView'
// 1. Define route components.
// These can be imported from other files

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: homeView},
  { path: '/ordem', component: listaOrdem },
  { path: '/cliente', component: listaClientes}
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})

export default router;
