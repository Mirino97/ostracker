<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Vue -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="https://unpkg.com/vue-router/dist/vue-router.js "></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">
</head>
<body>
    
    <div id="app">
        <modal></modal>
        <div class="menu">
            <div class="menu-header">
                <div class="menu-icon-div">
                    <img class="menu-icon" src="{{asset('/icons/menu.png')}}">
                </div>
                <div class="menu-logo">
                    <router-link to="/" style="color: white; margin-top: 16px;">OSTracker</router-link>
                </div>
            </div>
            <div class="menu-buttons">
                <router-link class="button" to="/ordem"><img src="{{asset('/icons/text-file-4-16.png')}}"> Ordens</router-link>
                <router-link class="button" to="/cliente"><img src="{{asset('/icons/user-16.png')}}"> Clientes</router-link>
                <router-link class="button" to="/financeiro"><img src="{{asset('/icons/us-dollar-16.png')}}"> Financeiro</router-link>             
            </div>
        </div>
        <div class="main-content-div">
            <nav class="navbar-custom">
                <div class="navbar-search-div">
                    {{-- <input class="navbar-search-input" type="text" placeholder="Heeeeeey"> --}}
                </div>
                <div class="user-menu">
                   <p class="user-menu-name">Olá, Júlio</p>
                   <img style="border-radius: 50%; height: 45px; width: 45px" src="{{asset('/icons/bunny2.jpeg')}}">
                </div>
            </nav>
    
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @elseif (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
            <main id="py-4">
                <flash-message></flash-message>
                <div id="main-content">
                    <router-view></router-view>
                </div>
            </main>
        </div>
    </div>

    <!-- Vue Main -->
    <script src="./js/app.js"></script>
    @yield('scripts')
</body>
</html>
