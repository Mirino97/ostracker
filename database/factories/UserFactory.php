<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Júlio César',
            'email' => 'j.cesarueti@yahoo.com.br',
            'email_verified_at' => now(),
            'password' => '32331408Aa',
            'remember_token' => Str::random(10),
        ];
    }
}
