<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nome' => Str::random(10),
            'cpf' => '12345678910',
            'endereco' => 'Rua Sabiá de Aparecida',
            'email' => Str::random(10).'@gmail.com',
            'telefone' => '(15)99999-9999',
        ]);
    }
}
